# Marketplace reactive

## Workflow

For development start the Spring Boot app `marketplace-reactive-app` in debug mode and run the `yarn watch` command 
in the `marketplace-reactive-ui` folder. This will automatically rebuild the Javascript/CSS/html 
artifacts when any code beneath the `src/main/frontend` is changed. Source maps and Live-reload are 
also enabled by default.

For deployment simply build the `marketplace-reactive-app` maven project via `mvn clean verify` and run the resulting jar. 
The artifact from the `marketplace-reactive-ui` module will be included in the generated fat-jar.

