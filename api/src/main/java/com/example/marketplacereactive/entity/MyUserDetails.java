package com.example.marketplacereactive.entity;

import java.util.Collection;
import static java.util.stream.Collectors.toList;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Data
@AllArgsConstructor
public class MyUserDetails implements UserDetails {
  private final User user;

  public String getUserId() {
    return user.getId();
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return user
      .getAuthorities()
      .stream()
      .map(SimpleGrantedAuthority::new)
      .collect(toList());
  }

  @Override
  public String getPassword() {
    return user.getPassword();
  }

  @Override
  public String getUsername() {
    return user.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return !user.isEnabled();
  }

  @Override
  public boolean isAccountNonLocked() {
    return !user.isEnabled();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return !user.isEnabled();
  }

  @Override
  public boolean isEnabled() {
    return user.isEnabled();
  }
}
