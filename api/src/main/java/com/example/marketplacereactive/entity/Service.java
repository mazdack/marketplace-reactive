package com.example.marketplacereactive.entity;

import java.net.URI;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Service {
  private String id;
  private URI endpoint;
  private String partnerAccountId;
}
