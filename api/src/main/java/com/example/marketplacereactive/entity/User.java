package com.example.marketplacereactive.entity;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
  private String id;
  private String username;
  private String password;

  private boolean enabled = true;
  private List<String> authorities;
}
