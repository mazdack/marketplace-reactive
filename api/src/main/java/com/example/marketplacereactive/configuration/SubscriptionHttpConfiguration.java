package com.example.marketplacereactive.configuration;

import com.example.marketplacereactive.entity.Subscription;
import com.example.marketplacereactive.repository.SubscriptionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import org.springframework.web.reactive.function.server.ServerResponse;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class SubscriptionHttpConfiguration {
  @Bean
  public RouterFunction<ServerResponse> subscriptionRoutes(SubscriptionRepository subscriptionRepository) {
    return route()
      .GET("/subscriptions", serverRequest ->
        ok().body(subscriptionRepository.findAll(), Subscription.class))
      .build();
  }
}
