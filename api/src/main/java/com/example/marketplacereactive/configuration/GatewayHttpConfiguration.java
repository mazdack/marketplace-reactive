package com.example.marketplacereactive.configuration;

import com.example.marketplacereactive.entity.MyUserDetails;
import com.example.marketplacereactive.entity.Service;
import com.example.marketplacereactive.entity.Subscription;
import com.example.marketplacereactive.repository.SubscriptionRepository;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.RouteToRequestUrlFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.URI_TEMPLATE_VARIABLES_ATTRIBUTE;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


@Log4j2
class SubscriptionFilter implements GatewayFilter, Ordered {

  private final SubscriptionRepository subscriptionRepository;

  SubscriptionFilter(SubscriptionRepository subscriptionRepository) {
    this.subscriptionRepository = subscriptionRepository;
  }

  @Override
  public int getOrder() {
    return RouteToRequestUrlFilter.ROUTE_TO_URL_FILTER_ORDER + 1;
  }

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    Map<String, String> variables = exchange.getAttribute(URI_TEMPLATE_VARIABLES_ATTRIBUTE);
    String subscriptionId = variables.get("subscriptionId");

    return exchange.getPrincipal()
      .map(Authentication.class::cast)
      .map(Authentication::getPrincipal)
      .map(MyUserDetails.class::cast)
      .map(MyUserDetails::getUserId)
      .flatMap(userId -> subscriptionRepository.findMySubscriptionById(subscriptionId, userId))
      .switchIfEmpty(Mono.defer(() -> {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Subscription not found. Id = " + subscriptionId);
      }))
      .map(Subscription::getService)
      .map(Service::getEndpoint)
      .doOnSuccess(endpoint -> {
        try {
          URI uri = replaceHost(exchange.getRequest().getURI(), endpoint);
          exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, uri);
        } catch (URISyntaxException e) {
          log.error(e);
        }
      })
      .then(chain.filter(exchange));
  }

  private static URI replaceHost(URI uri, URI newURI)
    throws URISyntaxException {

    uri = new URI(newURI.getScheme().toLowerCase(Locale.US), newURI.getAuthority(),
      newURI.getPath() + uri.getPath(), uri.getQuery(), uri.getFragment());

    return uri;
  }
}

@Configuration
@Log4j2
public class GatewayHttpConfiguration {
  @Bean
  SubscriptionFilter subscriptionFilter(SubscriptionRepository subscriptionRepository) {
    return new SubscriptionFilter(subscriptionRepository);
  }

  @Bean
  RouteLocator routes(RouteLocatorBuilder builder, SubscriptionFilter subscriptionFilter) {
    return builder
      .routes()
      .route(p -> p
        .path("/gateway/{subscriptionId:\\w+}/**")
        .filters(f -> f
//            .filter((exchange, chain) ->
//              exchange
//                .getPrincipal()
//                .switchIfEmpty(Mono.just(() -> "empty"))
//                .doOnSuccess(pr -> log.info("principal.name: {}", pr.getName()))
//                .then(chain.filter(exchange))
//            )
          .stripPrefix(2)
          .filter(subscriptionFilter)
        )
        .uri("no://op"))
      .build();
  }

}
