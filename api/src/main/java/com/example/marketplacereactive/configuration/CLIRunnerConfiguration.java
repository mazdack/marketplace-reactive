package com.example.marketplacereactive.configuration;

import com.example.marketplacereactive.entity.PartnerAccount;
import com.example.marketplacereactive.entity.Service;
import com.example.marketplacereactive.entity.Subscription;
import com.example.marketplacereactive.entity.User;
import com.example.marketplacereactive.repository.PartnerAccountRepository;
import com.example.marketplacereactive.repository.ServiceRepository;
import com.example.marketplacereactive.repository.SubscriptionRepository;
import com.example.marketplacereactive.repository.UserRepository;
import java.net.URI;
import java.net.URISyntaxException;
import static java.util.Collections.emptyList;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import reactor.core.publisher.Mono;

@Configuration
@Profile( "production" )
public class CLIRunnerConfiguration {
  private final UserRepository userRepository;
  private final PartnerAccountRepository partnerAccountRepository;
  private final ServiceRepository serviceRepository;
  private final SubscriptionRepository subscriptionRepository;
  private final PasswordEncoder passwordEncoder;

  public CLIRunnerConfiguration(UserRepository userRepository,
                                PartnerAccountRepository partnerAccountRepository,
                                ServiceRepository serviceRepository,
                                SubscriptionRepository subscriptionRepository,
                                PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.partnerAccountRepository = partnerAccountRepository;
    this.serviceRepository = serviceRepository;
    this.subscriptionRepository = subscriptionRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Bean
  public CommandLineRunner commandLineRunner() {
    return (args) -> {
      createPartnerAccount("provider")
        .map(partnerAccount -> {
          try {
            return new Service(null, new URI("https://httpbin.org/"), partnerAccount.getId());
          } catch (URISyntaxException e) {
            throw new RuntimeException(e);
          }
        })
        .flatMap(serviceRepository::save)
        .flatMap(this::createConsumerSubscription)
        .subscribe(System.out::println);
    };
  }

  private Mono<PartnerAccount> createPartnerAccount(String name) {
    return Mono
      .just(name)
      .map(username -> new User(null, username, passwordEncoder.encode("password"), true, emptyList()))
      .flatMap(userRepository::save)
      .doOnSuccess(System.out::println)
      .map(user -> new PartnerAccount(null, user.getUsername() + "'s partner account", user.getId()))
      .flatMap(partnerAccountRepository::save);
  }

  private Mono<Subscription> createConsumerSubscription(Service service) {
    return createPartnerAccount("consumer")
      .map(partnerAccount -> new Subscription(null, partnerAccount.getUserId(), service))
      .flatMap(subscriptionRepository::save);
  }

}
