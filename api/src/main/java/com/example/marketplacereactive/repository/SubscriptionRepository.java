package com.example.marketplacereactive.repository;


import com.example.marketplacereactive.entity.Subscription;
import org.reactivestreams.Publisher;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface SubscriptionRepository extends ReactiveCrudRepository<Subscription, String> {

  @Query("{'id': ?0, 'userId': ?1}")
  Mono<Subscription> findMySubscriptionById(String id, String userId);

}
