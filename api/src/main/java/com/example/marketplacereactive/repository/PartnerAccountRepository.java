package com.example.marketplacereactive.repository;

import com.example.marketplacereactive.entity.PartnerAccount;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface PartnerAccountRepository extends ReactiveCrudRepository<PartnerAccount, String> {
}
