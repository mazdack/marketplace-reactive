package com.example.marketplacereactive.repository;

import com.example.marketplacereactive.entity.Service;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ServiceRepository extends ReactiveCrudRepository<Service, String> {
}
