package com.example.marketplacereactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketplaceReactiveApi {

	public static void main(String[] args) {
		SpringApplication.run(MarketplaceReactiveApi.class, args);
	}

}
