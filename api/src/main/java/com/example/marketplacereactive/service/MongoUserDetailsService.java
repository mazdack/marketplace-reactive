package com.example.marketplacereactive.service;

import com.example.marketplacereactive.entity.MyUserDetails;
import com.example.marketplacereactive.repository.UserRepository;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Mono;

public class MongoUserDetailsService implements ReactiveUserDetailsService {
  private final UserRepository userRepository;

  public MongoUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public Mono<UserDetails> findByUsername(String username) {
    return userRepository
      .findByUsername(username)
      .map(MyUserDetails::new);
  }
}
