package com.example.marketplacereactive;

import com.example.marketplacereactive.entity.Service;
import com.example.marketplacereactive.entity.Subscription;
import com.example.marketplacereactive.entity.User;
import com.example.marketplacereactive.repository.SubscriptionRepository;
import com.example.marketplacereactive.repository.UserRepository;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.security.test.context.support.ReactorContextTestExecutionListener;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
@TestExecutionListeners(listeners = {
  GatewayServiceTest.class,
  DependencyInjectionTestExecutionListener.class,
  WithSecurityContextTestExecutionListener.class,
  ReactorContextTestExecutionListener.class
})
public class GatewayServiceTest extends AbstractTestExecutionListener {
  @Value("${wiremock.server.port}")
  private int wireMockPort;
  @Autowired
  private SubscriptionRepository subscriptionRepository;
  @Autowired
  private UserRepository userRepository;
  @LocalServerPort
  protected int marketplaceServicePort = 0;

  private WebTestClient client;
  private static User user;

  @Override
  public void beforeTestMethod(TestContext testContext) {
    UserRepository userRepository = testContext.getApplicationContext().getBean(UserRepository.class);

    Mono<User> user = Mono
      .just("user")
      .map(name -> new User(null, name, "password", true, emptyList()))
      .flatMap(userRepository::save)
      .doOnSuccess(user1 -> GatewayServiceTest.user = user1);

    StepVerifier
      .create(user)
      .expectNextMatches(u -> u.getId() != null)
      .verifyComplete();

  }

  @Override
  public int getOrder() {
    return 9999;
  }

  @Before
  public void setup() {
    String baseUri = "http://localhost:" + marketplaceServicePort;
    this.client = WebTestClient
      .bindToServer()
      .responseTimeout(Duration.ofSeconds(1000))
      .baseUrl(baseUri)
      .build();
  }

  @After
  public void tearDown() {
    Mono<Void> cleanUp = userRepository
      .deleteAll()
      .then(subscriptionRepository.deleteAll());

    StepVerifier
      .create(cleanUp)
      .verifyComplete();
  }

  @Test
  @WithUserDetails
  public void gateway() throws URISyntaxException {
    stubFor(get(urlEqualTo("/"))
      .willReturn(aResponse()
        .withBody("Hello, world!")
        .withHeader("Content-Type", "text/plain")));

    createSubscription("testId", user.getId());

    client
      .get()
      .uri("/gateway/testId")
      .exchange()
      .expectStatus().isOk()
      .expectBody()
      .consumeWith(response -> assertThat(response.getResponseBody()).contains("Hello".getBytes()));
  }

  @Test
  @WithUserDetails
  public void gatewayOtherUserSubscription() throws URISyntaxException {
    stubFor(get(urlEqualTo("/"))
      .willReturn(aResponse()
        .withBody("Hello, world!")
        .withHeader("Content-Type", "text/plain")));

    createSubscription("testId", "otherUserId");

    client
      .get()
      .uri("/gateway/testId")
      .exchange()
      .expectStatus().isNotFound();
  }

  @Test
  public void gatewayUnauthorized() {
    client.get().uri("/gateway/testId")
      .exchange()
      .expectStatus().isUnauthorized();
  }

  @Test
  @WithUserDetails
  public void gatewayPost() {
    client
      .post()
      .uri("/gateway/testId")
      .exchange()
      .expectStatus().isNotFound();
  }

  @Test
  @WithUserDetails
  public void gatewayWithPath() throws URISyntaxException {
    stubFor(get(urlEqualTo("/test"))
      .willReturn(aResponse()
        .withBody("Hello, path world!")
        .withHeader("Content-Type", "text/plain")));

    createSubscription("testId", user.getId());

    client.get().uri("/gateway/testId/test")
      .exchange()
      .expectStatus().isOk()
      .expectBody()
      .consumeWith(response -> assertThat(response.getResponseBody()).contains("path world".getBytes()));
  }

  private void createSubscription(String subscriptionId, String userId) throws URISyntaxException {
    URI endpoint = new URI("http://localhost:" + wireMockPort);

    Mono<Subscription> subscriptionMono = Mono
      .just(new Subscription(subscriptionId, userId, new Service("serviceId", endpoint, "partnerAccountId")))
      .flatMap(subscriptionRepository::save);

    StepVerifier
      .create(subscriptionMono)
      .expectNextMatches(subscription -> subscription.getId().equals(subscriptionId))
      .verifyComplete();
  }
}
