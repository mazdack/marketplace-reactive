package com.example.marketplacereactive;

import com.example.marketplacereactive.entity.Service;
import java.net.URI;
import java.net.URISyntaxException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@DataMongoTest
public class ServiceEntityTest {
  @Autowired
  private ReactiveMongoTemplate template;

  @Test
  public void persist() throws URISyntaxException {
    Service service = new Service(null, new URI("http://ya.ru"), "serviceName");
    Publisher<Service> save = this.template.save(service);

    StepVerifier
      .create(save)
      .expectNextMatches(s -> s.getId() != null && s.getEndpoint().getHost().equals("ya.ru"))
      .verifyComplete();
  }
}
