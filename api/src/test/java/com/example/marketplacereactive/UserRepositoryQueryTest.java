package com.example.marketplacereactive;


import com.example.marketplacereactive.entity.User;
import com.example.marketplacereactive.repository.UserRepository;
import java.util.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@DataMongoTest
public class UserRepositoryQueryTest {
  @Autowired
  private UserRepository userRepository;
  @Test
  public void query() {
    Flux<User> users = Flux
      .just("provider", "consumer")
      .map(username -> new User(null, username, "password", true, Collections.emptyList()))
      .flatMap(userRepository::save);

    StepVerifier
      .create(users)
      .expectNextCount(2l)
      .thenConsumeWhile(user -> user.getId() != null)
      .verifyComplete();
  }
}
