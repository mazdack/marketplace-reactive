package com.example.marketplacereactive;

import com.example.marketplacereactive.configuration.SubscriptionHttpConfiguration;
import com.example.marketplacereactive.entity.Service;
import com.example.marketplacereactive.entity.Subscription;
import com.example.marketplacereactive.repository.SubscriptionRepository;
import java.net.URI;
import java.net.URISyntaxException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

@RunWith(SpringRunner.class)
@WebFluxTest(value = {SubscriptionHttpConfiguration.class})
public class SubscriptionHttpTest {
  @MockBean
  private SubscriptionRepository subscriptionRepository;
  @Autowired
  private WebTestClient client;

  @Test
  @WithMockUser
  public void get() throws URISyntaxException {
    Mockito.when(subscriptionRepository.findAll()).thenReturn(Flux.just(
      mockSubscription("localhost"),
      mockSubscription("http://ya.ru")
    ));

    client
      .get()
      .uri("/subscriptions")
      .exchange()
      .expectStatus().isOk()
      .expectHeader().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
      .expectBody()
        .jsonPath("@.[0].service.endpoint").isEqualTo("localhost")
        .jsonPath("@.[1].service.endpoint").isEqualTo("http://ya.ru");
  }

  private Subscription mockSubscription(String url) throws URISyntaxException {
    URI endpoint = new URI(url);
    return new Subscription("testId", "userId", new Service("serviceId", endpoint, "partnerAccountId"));
  }

  @Test
  public void unauthorized() {
    client
      .get()
      .uri("/subscriptions")
      .exchange()
      .expectStatus().isUnauthorized();
  }
}
